import {Observable, throwError} from 'rxjs';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { AuthStorageService } from '../storage.service';
import { environment } from '../../environments/environment';
import * as jwt_decode from 'jwt-decode';

export class Token {
  refresh: string;
  access: string;
}

export class AccessToken {
  access: string;
}

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  constructor( private http: HttpClient, private storage: AuthStorageService) { }

  signInUser(username: string, password: string) {
    console.log('called signInUser auth service');
    const url = environment.base_url + environment.context + 'auth/token/obtain/';
    this.http.post<Token>(url, {'username': username, 'password': password}).subscribe(data => {
      this.storage.saveAccessToken(data.access);
      this.storage.saveRefreshToken(data.refresh);
    });
    return Observable.create( observer => {
        observer.next();
        observer.complete();
      }
    );
  }

  logoutUser() {
    this.storage.removeAuth();
    console.log('logout');
  }

  getTokenExpirationDate(token: string): Date {
    const decoded = jwt_decode(token);

    if (decoded.exp === undefined) {
      return null;
    }

    const date = new Date(0);
    date.setUTCSeconds(decoded.exp);
    return date;
  }

  isTokenExpired(token?: string): boolean {
    if (!token) {
      token = this.storage.getAccessToken();
    }
    if (!token) {
      return true;
    }

    const date = this.getTokenExpirationDate(token);
    if (date === undefined) {
      return false;
    }
    return !(date.valueOf() > new Date().valueOf());
  }

  hasAuthToken(): boolean {
    return !!this.storage.getAccessToken();
  }

  refreshAccessToken() {
    const url = environment.base_url + environment.context + 'auth/token/refresh/';
    this.http.post<AccessToken>(url, {'refresh': this.storage.getRefreshToken()}).subscribe(response => {
      console.log('New access token: ' + response.access);
      this.storage.saveAccessToken(response.access);
      return response.access;
    });
  }

  refreshAccessTokenObservable() {
    const url = environment.base_url + environment.context + 'auth/token/refresh/';
    return this.http.post<AccessToken>(url, {'refresh': this.storage.getRefreshToken()});
  }

  private handleError(error: HttpErrorResponse) {
    if (error.error instanceof ErrorEvent) {
      // A client-side or network error occurred. Handle it accordingly.
      console.error('An error occurred:', error.error.message);
    } else {
      // The backend returned an unsuccessful response code.
      // The response body may contain clues as to what went wrong,
      console.error(
        `Backend returned code ${error.status}, ` +
        `body was: ${error.error}`);
    }
    // return an observable with a user-facing error message
    return throwError(
      'Something bad happened; please try again later.');
  }
}

