import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { AuthService } from '../auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-signin',
  templateUrl: './signin.component.html',
  styleUrls: ['./signin.component.scss']
})
export class SigninComponent implements OnInit {

  constructor(private authService: AuthService, private router: Router) { }

  ngOnInit() {
  }

  onSignIn(form: NgForm) {
    const username = form.value.username;
    const password = form.value.password;
    console.log('called onSignIn component');
    this.authService.signInUser(username, password).subscribe( data => {
        this.router.navigate(['/home']);
      },
      error => {
        console.log('error on signin: ' + error);
      });
  }
}
