import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor
} from '@angular/common/http';
import { Observable } from 'rxjs';
import { AuthStorageService } from '../storage.service';
import { AuthService } from './auth.service';
import { Router } from '@angular/router';

@Injectable()
export class TokenInterceptor implements HttpInterceptor {
  constructor(public storage: AuthStorageService, private authService: AuthService, private router: Router) {}
  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    if (!request.url.endsWith('auth/token/obtain/') && !request.url.endsWith('auth/token/refresh/')) {
      if (this.authService.isTokenExpired(this.storage.getAccessToken())) {
        console.log('Token expired, refreshing token');
        const obs = this.authService.refreshAccessTokenObservable();
        obs.subscribe(response => {
            console.log('New access token: ' + response.access);
            this.storage.saveAccessToken(response.access);
            request = request.clone(
              {
                setHeaders: {Authorization: `Bearer ${this.storage.getAccessToken()}`}
              });
            return next.handle(request);
          }, err => {
            console.error('Could not refresh token:', err.message);
            this.router.navigate(['/signin']);
          },
          () => {
            console.log(`We're done here!`);
          });
      } else {
        console.log('cloning normally');
        request = request.clone(
          {
            setHeaders: {Authorization: `Bearer ${this.storage.getAccessToken()}`}
          });
        return next.handle(request);
      }
    }
    return next.handle(request);
  }
}
