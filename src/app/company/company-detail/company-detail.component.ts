import { Component, OnInit } from '@angular/core';
import { Company, CompanyService } from '../../services/company.service';
import { ActivatedRoute} from '@angular/router';


export interface PeriodicElement {
  name: string;
  position: number;
  weight: number;
  symbol: string;
}


@Component({
  selector: 'app-company-detail',
  templateUrl: './company-detail.component.html',
  styleUrls: ['./company-detail.component.scss']
})
export class CompanyDetailComponent implements OnInit {

  sub: any;
  company_id: number;
  company: Company;
  enable_progress: boolean;
  constructor(private companyService: CompanyService, private route: ActivatedRoute) {
    this.enable_progress = false;
  }

  ngOnInit() {
    this.sub = this.route.params.subscribe(params => {
      this.company_id = Number.parseInt(params['id']);
      console.log('getting company with id: ', this.company_id);
      this.getCompany(this.company_id);
    });
  }

  getCompany(id): void {
    this.companyService.getCompanyById(id)
      .subscribe(company => {
        this.company = company;
      });
  }

  updateFundamentus(): void {
    this.enable_progress = true;
    this.companyService.updateFundamentus(this.company_id).subscribe(data => {
      console.log(data);
      this.enable_progress = false;
    });
  }

  updateStock(): void {
    this.enable_progress = true;
    this.companyService.updateStock(this.company_id).subscribe(data => {
      console.log(data);
      this.enable_progress = false;
    });
  }

}
