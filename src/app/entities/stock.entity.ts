export interface Stock {
  id: number;
  date: Date;
  open: number;
  close: number;
  high: number;
  low: number;
  volume: number;
}
