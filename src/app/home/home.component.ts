import { Component, OnInit } from '@angular/core';
import { Company, CompanyService } from '../services/company.service';
import { OrderService} from '../services/order.service';
import { OrderCompany } from '../order/order.interface';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  companies: Company[];
  order_companies: OrderCompany[];

  constructor(private companyService: CompanyService,
              private orderCompanyService: OrderService) { }

  ngOnInit() {
  }

  getCompanies(): void {
    this.companyService.getCompanies()
      .subscribe(companies => this.companies = companies);
  }

  getOrderingCompanies(): void {
    this.orderCompanyService.getOrderCompanies()
      .subscribe(order_companies => this.order_companies = order_companies);
  }

  updateCompaniesFundamentus(): void {
    this.companyService.updateCompaniesFundamentus()
      .subscribe(data => {
        console.log(data);
      });
  }

  updateCompaniesStock(): void {
    this.companyService.updateCompaniesStock()
      .subscribe(data => {
        console.log(data);
      });
  }

}
