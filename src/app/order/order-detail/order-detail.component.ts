import { Component, Inject, LOCALE_ID, OnInit } from '@angular/core';
import { OrderService } from '../../services/order.service';
import { Order } from '../order.interface';
import { ActivatedRoute, Router } from '@angular/router';
import { FormBuilder, FormGroup } from '@angular/forms';
import { formatNumber } from '@angular/common';

@Component({
  selector: 'app-order-detail',
  templateUrl: './order-detail.component.html',
  styleUrls: ['./order-detail.component.scss']
})
export class OrderDetailComponent implements OnInit {

  sub: any;
  isNewOrder: boolean;
  order_id: number;
  order: Order;
  enable_progress: boolean;
  orderForm: FormGroup;
  name: string;
  // Indicadores Fundamentalistas
  pl_checked: boolean;
  pl: number;
  pvp: number;
  pvp_checked: boolean;
  pebit: number;
  pebit_checked: boolean;
  psr_checked: boolean;
  psr: number;
  at_checked: boolean;
  at: number;
  p_ativ_circ_liq_checked: boolean;
  p_ativ_circ_liq: number;
  div_yield_checked: boolean;
  div_yield: number;
  ev_ebit_checked: boolean;
  ev_ebit: number;
  giro_ativ_checked: boolean;
  giro_ativ: number;
  div_br_patrim_checked: boolean;
  div_br_patrim: number;
  cresc_rec_5_a_checked: boolean;
  cresc_rec_5_a: number;
  lpa: number;
  lpa_checked: boolean;
  vpa: number;
  vpa_checked: boolean;
  mb: number;
  mb_checked: boolean;
  me: number;
  me_checked: boolean;
  ml: number;
  ml_checked: boolean;
  ebita: number;
  ebita_checked: boolean;
  roic: number;
  roic_checked: boolean;
  roe: number;
  roe_checked: boolean;
  liquidez_corr: number;
  liquidez_corr_checked: boolean;

  // Balanco Patrimonial
  ativo: number;
  ativo_checked: boolean;
  disponibilidades: number;
  disponibilidades_checked: boolean;
  ativo_circulante: number;
  ativo_circulante_checked: boolean;

  div_bruta: number;
  div_bruta_checked: boolean;
  div_liquida: number;
  div_liquida_checked: boolean;
  patr_liquido: number;
  patr_liquido_checked: boolean;

  // Demonstrativo de resultados
  receita_liquida_12_meses: number;
  receita_liquida_12_meses_checked: boolean;
  ebit_12_meses: number;
  ebit_12_meses_checked: boolean;
  lucro_liquido_12_meses: number;
  lucro_liquido_12_meses_checked: boolean;

  receita_liquida_3_meses: number;
  receita_liquida_3_meses_checked: boolean;
  ebit_3_meses: number;
  ebit_3_meses_checked: boolean;
  lucro_liquido_3_meses: number;
  lucro_liquido_3_meses_checked: boolean;

  constructor(private orderService: OrderService, private route: ActivatedRoute, fb: FormBuilder,
              @Inject(LOCALE_ID) private locale: string, private router: Router) {
    this.orderForm = fb.group({
      name: null,
      // Indicadores Fundamentalistas
      pl: 0.0,
      pl_checked: false,
      pvp: 0.0,
      pvp_checked: false,
      pebit: 0.0,
      pebit_checked: false,

      psr_checked: false,
      psr: 0.0,
      at_checked: false,
      at: 0.0,
      pcg_checked: false,
      pcg: 0.0,

      p_ativ_circ_liq_checked: false,
      p_ativ_circ_liq: 0.0,
      div_yield_checked: false,
      div_yield: 0.0,
      ev_ebit_checked: false,
      ev_ebit: 0.0,

      giro_ativ_checked: false,
      giro_ativ: 0.0,
      div_br_patrim_checked: false,
      div_br_patrim: 0.0,
      cresc_rec_5_a_checked: false,
      cresc_rec_5_a: 0.0,

      lpa: 0.0,
      lpa_checked: false,
      vpa: 0.0,
      vpa_checked: false,
      mb: 0.0,
      mb_checked: false,

      me: 0.0,
      me_checked: false,
      ml: 0.0,
      ml_checked: false,
      ebita: 0.0,
      ebita_checked: false,

      roic: 0.0,
      roic_checked: false,
      roe: 0.0,
      roe_checked: false,
      liquidez_corr: 0.0,
      liquidez_corr_checked: false,

      // Balanco Patrimonial
      ativo: 0.0,
      ativo_checked: false,
      disponibilidades: 0.0,
      disponibilidades_checked: false,
      ativo_circulante: 0.0,
      ativo_circulante_checked: false,

      div_bruta: 0.0,
      div_bruta_checked: false,
      div_liquida: 0.0,
      div_liquida_checked: false,
      patr_liquido: 0.0,
      patr_liquido_checked: false,

      receita_liquida_12_meses: 0.0,
      receita_liquida_12_meses_checked: false,
      ebit_12_meses: 0.0,
      ebit_12_meses_checked: false,
      lucro_liquido_12_meses: 0.0,
      lucro_liquido_12_meses_checked: false,

      receita_liquida_3_meses: 0.0,
      receita_liquida_3_meses_checked: false,
      ebit_3_meses: 0.0,
      ebit_3_meses_checked: false,
      lucro_liquido_3_meses: 0.0,
      lucro_liquido_3_meses_checked: false,
    });
  }

  ngOnInit() {
    this.enable_progress = true;
    this.sub = this.route.params.subscribe(params => {
      if (params['id'] === 'new') {
        this.isNewOrder = true;
        this.enable_progress = false;
      } else {
        this.isNewOrder = false;
        this.order_id = Number.parseInt(params['id']);
        this.orderService.getOrderById(this.order_id ).subscribe( data => {
            this.order = data;
            this.enable_progress = false;
            this.orderForm.get('name').setValue(this.order.name);
            this.orderForm.get('pl').setValue(this.order.pl);
            this.orderForm.get('pl_checked').setValue(!this.order.disable_pl);
            this.orderForm.get('pvp').setValue(this.order.pvp);
            this.orderForm.get('pvp_checked').setValue(!this.order.disable_pvp);
            this.orderForm.get('pebit').setValue(this.order.pebit);
            this.orderForm.get('pebit_checked').setValue(!this.order.disable_pebit);
            this.orderForm.get('psr_checked').setValue(!this.order.disable_psr);
            this.orderForm.get('psr').setValue(this.order.psr);
            this.orderForm.get('at_checked').setValue(!this.order.disable_at);
            this.orderForm.get('at').setValue(this.order.at);
            this.orderForm.get('pcg_checked').setValue(!this.order.disable_pcg);
            this.orderForm.get('pcg').setValue(this.order.pcg);

            this.orderForm.get('p_ativ_circ_liq_checked').setValue(!this.order.disable_p_ativ_circ_liq);
            this.orderForm.get('p_ativ_circ_liq').setValue(this.order.p_ativ_circ_liq);
            this.orderForm.get('div_yield_checked').setValue(!this.order.disable_div_yield);
            this.orderForm.get('div_yield').setValue(this.order.div_yield);
            this.orderForm.get('ev_ebit_checked').setValue(!this.order.disable_ev_ebit);
            this.orderForm.get('ev_ebit').setValue(this.order.ev_ebit);

            this.orderForm.get('giro_ativ_checked').setValue(!this.order.disable_giro_ativ);
            this.orderForm.get('giro_ativ').setValue(this.order.giro_ativ);
            this.orderForm.get('div_br_patrim_checked').setValue(!this.order.disable_div_br_patrim);
            this.orderForm.get('div_br_patrim').setValue(this.order.div_br_patrim);
            this.orderForm.get('cresc_rec_5_a_checked').setValue(!this.order.disable_cresc_rec_5_a);
            this.orderForm.get('cresc_rec_5_a').setValue(this.order.cresc_rec_5_a);

            this.orderForm.get('lpa').setValue(this.order.lpa);
            this.orderForm.get('lpa_checked').setValue(!this.order.disable_lpa);
            this.orderForm.get('vpa').setValue(this.order.vpa);
            this.orderForm.get('vpa_checked').setValue(!this.order.disable_vpa);
            this.orderForm.get('mb').setValue(this.order.mb);
            this.orderForm.get('mb_checked').setValue(!this.order.disable_mb);

            this.orderForm.get('me').setValue(this.order.me);
            this.orderForm.get('me_checked').setValue(!this.order.disable_me);
            this.orderForm.get('ml').setValue(this.order.ml);
            this.orderForm.get('ml_checked').setValue(!this.order.disable_ml);
            this.orderForm.get('ebita').setValue(this.order.ebita);
            this.orderForm.get('ebita_checked').setValue(!this.order.disable_ebita);

            this.orderForm.get('roic').setValue(this.order.roic);
            this.orderForm.get('roic_checked').setValue(!this.order.disable_roic);
            this.orderForm.get('roe').setValue(this.order.roe);
            this.orderForm.get('roe_checked').setValue(!this.order.disable_roe);
            this.orderForm.get('liquidez_corr').setValue(this.order.liquidez_corr);
            this.orderForm.get('liquidez_corr_checked').setValue(!this.order.disable_liquidez_corr);

            // Balanco Patrimonial
            this.orderForm.get('ativo').setValue(this.order.ativo);
            this.orderForm.get('ativo_checked').setValue(!this.order.disable_ativo);
            this.orderForm.get('disponibilidades').setValue(this.order.disponibilidades);
            this.orderForm.get('disponibilidades_checked').setValue(!this.order.disable_disponibilidades);
            this.orderForm.get('ativo_circulante').setValue(this.order.ativo_circulante);
            this.orderForm.get('ativo_circulante_checked').setValue(!this.order.disable_ativo_circulante);

            this.orderForm.get('div_bruta').setValue(this.order.div_bruta);
            this.orderForm.get('div_bruta_checked').setValue(!this.order.disable_div_bruta);
            this.orderForm.get('div_liquida').setValue(this.order.div_liquida);
            this.orderForm.get('div_liquida_checked').setValue(!this.order.disable_div_liquida);
            this.orderForm.get('patr_liquido').setValue(this.order.patr_liquido);
            this.orderForm.get('patr_liquido_checked').setValue(!this.order.disable_patr_liquido);

            this.orderForm.get('receita_liquida_12_meses').setValue(this.order.receita_liquida_12_meses);
            this.orderForm.get('receita_liquida_12_meses_checked').setValue(!this.order.disable_receita_liquida_12_meses);
            this.orderForm.get('ebit_12_meses').setValue(this.order.ebit_12_meses);
            this.orderForm.get('ebit_12_meses_checked').setValue(!this.order.disable_ebit_12_meses);
            this.orderForm.get('lucro_liquido_12_meses').setValue(this.order.lucro_liquido_12_meses);
            this.orderForm.get('lucro_liquido_12_meses_checked').setValue(!this.order.disable_lucro_liquido_12_meses);

            this.orderForm.get('receita_liquida_3_meses').setValue(this.order.receita_liquida_3_meses);
            this.orderForm.get('receita_liquida_3_meses_checked').setValue(!this.order.disable_receita_liquida_3_meses);
            this.orderForm.get('ebit_3_meses').setValue(this.order.ebit_3_meses);
            this.orderForm.get('ebit_3_meses_checked').setValue(!this.order.disable_ebit_3_meses);
            this.orderForm.get('lucro_liquido_3_meses').setValue(this.order.lucro_liquido_3_meses);
            this.orderForm.get('lucro_liquido_3_meses_checked').setValue(!this.order.disable_lucro_liquido_3_meses);
          }
        );
      }
    });
  }

  onSubmit() {
    const new_order = <Order>{};
    new_order.name = this.orderForm.get('name').value;
    new_order.pl = formatNumber(this.orderForm.get('pl').value, this.locale, '1.3');
    new_order.disable_pl = !this.orderForm.get('pl_checked').value;
    new_order.pvp = formatNumber(this.orderForm.get('pvp').value, this.locale, '1.3');
    new_order.disable_pvp = !this.orderForm.get('pvp_checked').value;
    new_order.pebit = formatNumber(this.orderForm.get('pebit').value, this.locale, '1.3');
    new_order.disable_pebit = !this.orderForm.get('pebit_checked').value;
    new_order.disable_psr = !this.orderForm.get('psr_checked').value;
    new_order.psr = formatNumber(this.orderForm.get('psr').value, this.locale, '1.3');
    new_order.disable_at = !this.orderForm.get('at_checked').value;
    new_order.at = formatNumber(this.orderForm.get('at').value, this.locale, '1.3');
    new_order.disable_pcg = !this.orderForm.get('pcg_checked').value;
    new_order.pcg = formatNumber(this.orderForm.get('pcg').value, this.locale, '1.3');
    new_order.disable_p_ativ_circ_liq = !this.orderForm.get('p_ativ_circ_liq_checked').value;
    new_order.p_ativ_circ_liq = formatNumber(this.orderForm.get('p_ativ_circ_liq').value, this.locale, '1.3');
    new_order.disable_div_yield = !this.orderForm.get('div_yield_checked').value;
    new_order.div_yield = formatNumber(this.orderForm.get('div_yield').value, this.locale, '1.3');
    new_order.disable_ev_ebit = !this.orderForm.get('ev_ebit_checked').value;
    new_order.ev_ebit = formatNumber(this.orderForm.get('ev_ebit').value, this.locale, '1.3');

    new_order.disable_giro_ativ = !this.orderForm.get('giro_ativ_checked').value;
    new_order.giro_ativ = formatNumber(this.orderForm.get('giro_ativ').value, this.locale, '1.3');
    new_order.disable_div_br_patrim = !this.orderForm.get('div_br_patrim_checked').value;
    new_order.div_br_patrim = formatNumber(this.orderForm.get('div_br_patrim').value, this.locale, '1.3');
    new_order.disable_cresc_rec_5_a = !this.orderForm.get('cresc_rec_5_a_checked').value;
    new_order.cresc_rec_5_a = formatNumber(this.orderForm.get('cresc_rec_5_a').value, this.locale, '1.3');

    new_order.lpa = formatNumber(this.orderForm.get('lpa').value, this.locale, '1.3');
    new_order.disable_lpa = !this.orderForm.get('lpa_checked').value;
    new_order.vpa = formatNumber(this.orderForm.get('vpa').value, this.locale, '1.3');
    new_order.disable_vpa = !this.orderForm.get('vpa_checked').value;
    new_order.mb = formatNumber(this.orderForm.get('mb').value, this.locale, '1.3');
    new_order.disable_mb = !this.orderForm.get('mb_checked').value;

    new_order.me = formatNumber(this.orderForm.get('me').value, this.locale, '1.3');
    new_order.disable_me = !this.orderForm.get('me_checked').value;
    new_order.ml = formatNumber(this.orderForm.get('ml').value, this.locale, '1.3');
    new_order.disable_ml = !this.orderForm.get('ml_checked').value;
    new_order.ebita = formatNumber(this.orderForm.get('ebita').value, this.locale, '1.3');
    new_order.disable_ebita = !this.orderForm.get('ebita_checked').value;

    new_order.roic = formatNumber(this.orderForm.get('roic').value, this.locale, '1.3');
    new_order.disable_roic = !this.orderForm.get('roic_checked').value;
    new_order.roe = formatNumber(this.orderForm.get('roe').value, this.locale, '1.3');
    new_order.disable_roe = !this.orderForm.get('roe_checked').value;
    new_order.liquidez_corr = formatNumber(this.orderForm.get('liquidez_corr').value, this.locale, '1.3');
    new_order.disable_liquidez_corr = !this.orderForm.get('liquidez_corr_checked').value;

    new_order.ativo = formatNumber(this.orderForm.get('ativo').value, this.locale, '1.3');
    new_order.disable_ativo = !this.orderForm.get('ativo_checked').value;
    new_order.disponibilidades = formatNumber(this.orderForm.get('disponibilidades').value, this.locale, '1.3');
    new_order.disable_disponibilidades = !this.orderForm.get('disponibilidades_checked').value;
    new_order.ativo_circulante = formatNumber(this.orderForm.get('ativo_circulante').value, this.locale, '1.3');
    new_order.disable_ativo_circulante = !this.orderForm.get('ativo_circulante_checked').value;

    new_order.div_bruta = formatNumber(this.orderForm.get('div_bruta').value, this.locale, '1.3');
    new_order.disable_div_bruta = !this.orderForm.get('div_bruta_checked').value;
    new_order.div_liquida = formatNumber(this.orderForm.get('div_liquida').value, this.locale, '1.3');
    new_order.disable_div_liquida = !this.orderForm.get('div_liquida_checked').value;
    new_order.patr_liquido = formatNumber(this.orderForm.get('patr_liquido').value, this.locale, '1.3');
    new_order.disable_patr_liquido = !this.orderForm.get('patr_liquido_checked').value;

    new_order.receita_liquida_12_meses = formatNumber(this.orderForm.get('receita_liquida_12_meses').value, this.locale, '1.3');
    new_order.disable_receita_liquida_12_meses = !this.orderForm.get('receita_liquida_12_meses_checked').value;
    new_order.ebit_12_meses = formatNumber(this.orderForm.get('ebit_12_meses').value, this.locale, '1.3');
    new_order.disable_ebit_12_meses = !this.orderForm.get('ebit_12_meses_checked').value;
    new_order.lucro_liquido_12_meses = formatNumber(this.orderForm.get('lucro_liquido_12_meses').value, this.locale, '1.3');
    new_order.disable_lucro_liquido_12_meses = !this.orderForm.get('lucro_liquido_12_meses_checked').value;

    new_order.receita_liquida_3_meses = formatNumber(this.orderForm.get('receita_liquida_3_meses').value, this.locale, '1.3');
    new_order.disable_receita_liquida_3_meses = !this.orderForm.get('receita_liquida_3_meses_checked').value;
    new_order.ebit_3_meses = formatNumber(this.orderForm.get('ebit_3_meses').value, this.locale, '1.3');
    new_order.disable_ebit_3_meses = !this.orderForm.get('ebit_3_meses_checked').value;
    new_order.lucro_liquido_3_meses = formatNumber(this.orderForm.get('lucro_liquido_3_meses').value, this.locale, '1.3');
    new_order.disable_lucro_liquido_3_meses = !this.orderForm.get('lucro_liquido_3_meses_checked').value;

    if (this.isNewOrder) {
      this.orderService.saveOrder(new_order).subscribe(data => {
        console.log(data);
        this.router.navigate(['/order']);
      });
    } else {
      this.orderService.updateOrder(this.order_id, new_order).subscribe(data => {
        console.log(data);
      });
    }
  }
}
