import { Component, OnInit } from '@angular/core';
import { Order } from './order.interface';
import {OrderService} from '../services/order.service';

@Component({
  selector: 'app-order',
  templateUrl: './order.component.html',
  styleUrls: ['./order.component.scss']
})
export class OrderComponent implements OnInit {

  orders: Order[];
  enable_progress: boolean;
  constructor(private orderService: OrderService) { }

  ngOnInit() {
    this.enable_progress = true;
    this.orderService.getOrders().subscribe( data => {
      this.orders = data;
      console.log(this.orders);
      this.enable_progress = false;
      }
    );
  }

}
