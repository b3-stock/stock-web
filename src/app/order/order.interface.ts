export interface Order {
  // id: number;
  name: string;
  // Indicadores Fundamentalistas
  pl: string;
  disable_pl: boolean;
  lpa: string;
  disable_lpa: boolean;
  pvp: string;
  disable_pvp: boolean;
  vpa: string;
  disable_vpa: boolean;
  pebit: string;
  disable_pebit: boolean;
  mb: string;
  disable_mb: boolean;
  psr: string;
  disable_psr: boolean;
  me: string;
  disable_me: boolean;
  at: string;
  disable_at: boolean;
  ml: string;
  disable_ml: boolean;
  pcg: string;
  disable_pcg: boolean;
  ebita: string;
  disable_ebita: boolean;
  p_ativ_circ_liq: string;
  disable_p_ativ_circ_liq: boolean;
  roic: string;
  disable_roic: boolean;
  div_yield: string;
  disable_div_yield: boolean;
  roe: string;
  disable_roe: boolean;
  ev_ebit: string;
  disable_ev_ebit: boolean;
  liquidez_corr: string;
  disable_liquidez_corr: boolean;
  giro_ativ: string;
  disable_giro_ativ: boolean;
  div_br_patrim: string;
  disable_div_br_patrim: boolean;
  cresc_rec_5_a: string;
  disable_cresc_rec_5_a: boolean;
  // Balanco patrimonial
  ativo: string;
  disable_ativo: boolean;
  div_bruta: string;
  disable_div_bruta: boolean;
  disponibilidades: string;
  disable_disponibilidades: boolean;
  div_liquida: string;
  disable_div_liquida: boolean;
  ativo_circulante: string;
  disable_ativo_circulante: boolean;
  patr_liquido: string;
  disable_patr_liquido: boolean;

  // Demonstrativo de resultados
  receita_liquida_12_meses: string;
  disable_receita_liquida_12_meses: boolean;
  ebit_12_meses: string;
  disable_ebit_12_meses: boolean;
  lucro_liquido_12_meses: string;
  disable_lucro_liquido_12_meses: boolean;

  receita_liquida_3_meses: string;
  disable_receita_liquida_3_meses: boolean;
  ebit_3_meses: string;
  disable_ebit_3_meses: boolean;
  lucro_liquido_3_meses: string;
  disable_lucro_liquido_3_meses: boolean;
}

export class OrderCompany {
  id: number;
  code: string;
  name: string;
  last_negotiation_date: string;
  last_indicator_release_date: string;
  grade: number;
  pl: number;
  disable: boolean;
}

export interface ResponseOrder extends Order {
  id: number;
}
