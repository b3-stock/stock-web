import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { catchError } from 'rxjs/internal/operators';
import { environment } from '../../environments/environment';
import { Indicator } from '../entities/indicator.entity';
import { Stock } from '../entities/stock.entity';


export class Company {
  id: number;
  code: string;
  name: string;
  paper_type: string;
  sector: string;
  sub_sector: string;
  last_negotiation_date: string;
  disable: boolean;
  last_indicator: Indicator;
  last_stock: Stock;
  last_stock_month: Stock[];
}


@Injectable({
  providedIn: 'root'
})
export class CompanyService {
  constructor( private http: HttpClient) { }
  getCompanies(): Observable<Company[]> {
    const url = environment.base_url + environment.context + 'company/get-companies/';
    return this.http.get<Company[]>(url)
      .pipe(
        catchError(this.handleError)
      );
  }

  getCompanyById(id: number): Observable<Company> {
    const url = environment.base_url + environment.context + 'company/' + id + '/';
    return this.http.get<Company>(url)
      .pipe(
        catchError(this.handleError)
      );
  }

  updateCompaniesFundamentus(): Observable<string> {
    const url = environment.base_url + environment.context + 'company/update-companies-fundamentus/';
    return this.http.post<''>(url, {})
      .pipe(
        catchError(this.handleError)
      );
  }

  updateFundamentus(id: number): Observable<string> {
    const url = environment.base_url + environment.context + 'company/' + id + '/update-company-fundamentus/';
    return this.http.post<''>(url, {})
      .pipe(
        catchError(this.handleError)
      );
  }

  updateStock(id: number): Observable<string> {
    const url = environment.base_url + environment.context + 'company/' + id + '/update-stock/';
    return this.http.post<''>(url, {})
      .pipe(
        catchError(this.handleError)
      );
  }

  updateCompaniesStock(): Observable<string> {
    const url = environment.base_url + environment.context + 'company/update-companies-stock/';
    return this.http.post<''>(url, {})
      .pipe(
        catchError(this.handleError)
      );
  }

  private handleError(error: HttpErrorResponse) {
    if (error.error instanceof ErrorEvent) {
      // A client-side or network error occurred. Handle it accordingly.
      console.error('An error occurred:', error.error.message);
    } else {
      // The backend returned an unsuccessful response code.
      // The response body may contain clues as to what went wrong,
      console.error(
        `Backend returned code ${error.status}, ` +
        `body was: ${JSON.stringify(error.error)}`);
    }
    // return an observable with a user-facing error message
    return throwError(
      'Something bad happened; please try again later.');
  }
}
