import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { catchError } from 'rxjs/internal/operators';
import { environment } from '../../environments/environment';
import { Order, OrderCompany, ResponseOrder } from '../order/order.interface';


@Injectable({
  providedIn: 'root'
})
export class OrderService {
  constructor( private http: HttpClient) { }

  getOrderCompanies(): Observable<OrderCompany[]> {
    const url = environment.base_url + environment.context + 'order_company/1/get-companies-in-order/';
    return this.http.get<OrderCompany[]>(url)
      .pipe(catchError(this.handleError));
  }

  getOrders(): Observable<ResponseOrder[]> {
    const url = environment.base_url + environment.context + 'order_company/';
    return this.http.get<ResponseOrder[]>(url)
      .pipe(catchError(this.handleError));
  }

  getOrderById(id: number): Observable<ResponseOrder> {
    const url = environment.base_url + environment.context + 'order_company/' + id + '/';
    return this.http.get<ResponseOrder>(url)
      .pipe(
        catchError(this.handleError)
      );
  }
  saveOrder(order: Order): Observable<any> {
    const url = environment.base_url + environment.context + 'order_company/';
    return this.http.post<Order>(url, order)
      .pipe(
        catchError(this.handleError)
      );
  }
  updateOrder(id, order: Order): Observable<any> {
    const url = environment.base_url + environment.context + 'order_company/' + id + '/';
    return this.http.put<Order>(url, order)
      .pipe(
        catchError(this.handleError)
      );
  }


  private handleError(error: HttpErrorResponse) {
    if (error.error instanceof ErrorEvent) {
      // A client-side or network error occurred. Handle it accordingly.
      console.error('An error occurred:', error.error.message);
    } else {
      // The backend returned an unsuccessful response code.
      // The response body may contain clues as to what went wrong,
      console.error(
        `Backend returned code ${error.status}, ` +
        `body was: ${JSON.stringify(error.error)}`);
    }
    // return an observable with a user-facing error message
    return throwError(
      'Something bad happened; please try again later.');
  }
}
