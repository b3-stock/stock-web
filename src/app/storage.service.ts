import {Injectable} from '@angular/core';
import {LocalStorageService} from 'ngx-store';

@Injectable({
  providedIn: 'root'
})
export class AuthStorageService {
  constructor(public localStorageService: LocalStorageService) { }

  saveAccessToken(value) {
    this.localStorageService.set('access_token', value);
  }
  getAccessToken() {
    return this.localStorageService.get('access_token');
  }
  saveRefreshToken(value) {
    this.localStorageService.set('refresh_token', value);
  }
  getRefreshToken() {
    return this.localStorageService.get('refresh_token');
  }
  removeAuth() {
    this.localStorageService.remove('access_token');
    this.localStorageService.remove('refresh_token');
  }

}

